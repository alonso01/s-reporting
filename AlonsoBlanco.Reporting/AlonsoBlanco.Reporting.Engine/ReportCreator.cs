﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using System.IO;

namespace AlonsoBlanco.Reporting.Engine
{
    public class ReportCreator
    {

        public static MemoryStream Create(string name, string query)
        {

            int result = 0;
            List<List<object>> reportInfo = new List<List<object>>();
            List<string> columnNames = new List<string>();
            string sql = query;


            #region get report data from the db
            
            // Configure the DatabaseFactory to read its configuration from the .config file
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            
            // Create a Database object from the factory using the connection string name.
            Database db = factory.Create("DefaultConnection");
            
            var queryDataSet = db.ExecuteDataSet(CommandType.Text, sql);

            if(queryDataSet != null && queryDataSet.Tables.Count > 0)
            {

                foreach(DataRow row in queryDataSet.Tables[0].Rows)
                {

                    List<object> rowData = row.ItemArray.ToList();
                    reportInfo.Add(rowData);

                }

                foreach(DataColumn col in queryDataSet.Tables[0].Columns)
                {
                    columnNames.Add(col.ColumnName);
                }

            }

            #endregion

            #region Generate Report Output

            if(reportInfo != null && reportInfo.Count > 0)
            {

                //create base output
                
                string template = Resource1.Tabular1;

                template = template.Replace("{name}", name);

                StringBuilder htmlTableData = new StringBuilder();

                htmlTableData.Append("<table><thead><tr>");

                foreach (string colName in columnNames)
                {
                    htmlTableData.Append("<th>" + colName + "</th>");
                }

                htmlTableData.Append("</tr></thead><tbody>");

                foreach (var rowObjList in reportInfo)
                {
                    htmlTableData.Append("<tr>");
                    foreach (var rowObj in rowObjList)
                    {
                        htmlTableData.Append("<td>" + rowObj.ToString() + "</td>");
                    }
                    htmlTableData.Append("</tr>");
                }

                htmlTableData.Append("</tbody></table>");

                template = template.Replace("{tableData}", htmlTableData.ToString());

                //create stream with the desired output

                return GenerateStreamFromString(template);
                

            }


            #endregion


            return null;

        }

        private static MemoryStream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

    }
}
