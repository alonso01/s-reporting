﻿using AlonsoBlanco.Reporting.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AlonsoBlanco.Reporting.Controllers
{
    public class HomeController : Controller
    {

        [Authorize(Users = "admin")]
        public ActionResult Index()
        {

            var reportElements = ReportsManager.All();

            var reportsModel = new List<ReportModel>();

            foreach(var reportEl in reportElements.Values)
            {
                reportsModel.Add(
                    new ReportModel
                    {
                        Key = reportEl.Key,
                        Name = reportEl.Name,
                        Query = reportEl.Query,
                        Output = reportEl.Output
                    }
               );
            }

            return View(reportsModel);

        }
        
       
    }
}