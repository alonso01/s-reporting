﻿using AlonsoBlanco.Reporting.Engine;
using SelectPdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace AlonsoBlanco.Reporting.Controllers
{

    [AllowAnonymous]
    public class ReportApiController : Controller
    {


        public ActionResult View(string reportId)
        {

            string baseUrl = System.Configuration.ConfigurationManager.AppSettings["baseUrl"];
            string destinationFolder = System.Configuration.ConfigurationManager.AppSettings["destinationFolder"];

            if (ReportsManager.IsValidReport(reportId))
            {
                var reportParams = ReportsManager.Instances(reportId);

                var reportStream = ReportCreator.Create(reportParams.Name, reportParams.Query);

                reportStream.WriteTo(Response.OutputStream);

                reportStream.Flush();
                reportStream.Close();
                
            }else
            {
                ViewBag.Id = reportId;
                return View("ReportNotFound");
            }

            return View();

        }

        // GET: ReportApi
        public FileResult Export(string reportId, string output, string apiKey)
        {


            string baseUrl = System.Configuration.ConfigurationManager.AppSettings["baseUrl"];
            string exportApiKey = System.Configuration.ConfigurationManager.AppSettings["exportApiKey"];
            string destinationFolder = System.Configuration.ConfigurationManager.AppSettings["destinationFolder"];

            if( (!string.IsNullOrEmpty(exportApiKey) && !string.IsNullOrEmpty(apiKey)) && apiKey != exportApiKey)
            {

                return null;
            }

            
            if (string.IsNullOrEmpty(output))
            {
                output = "pdf";
            }
            
            if(output == "pdf")
            {
                
                // instantiate the html to pdf converter 
                HtmlToPdf converter = new HtmlToPdf();

                string pdfFileName = "Report-" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";

                // convert the url to pdf 
                PdfDocument doc = converter.ConvertUrl(baseUrl + "ReportApi/View?print=false&output=pdf&reportId="+reportId);
                string pdfPath = destinationFolder + pdfFileName;
                // save pdf document 
                doc.Save(pdfPath);

                // close pdf document 
                doc.Close();

                //Response.ContentType = "application/pdf";
                //Response.ContentEncoding = System.Text.Encoding.UTF8;
                //Response.AddHeader(@"Content-Disposition", "attachment; filename=" + pdfPath);

                //Response.Flush();
                //Response.Close();
                //Response.End();

                return File(pdfPath, "application/pdf", pdfFileName);

            }
            else
            {

                #region create xls attachment
                var htmlReportData = string.Empty;
                var xlsPath = string.Empty;
                try
                {

                    string xlsFileName = "Report-" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls";

                    xlsPath = destinationFolder + xlsFileName;
                    htmlReportData = new System.Net.Http.HttpClient().GetStringAsync(baseUrl + "ReportApi/View?print=false&output=pdf&reportId=" + reportId).Result;
                    System.IO.File.WriteAllText(xlsPath, htmlReportData);

                    //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.ContentEncoding = System.Text.Encoding.UTF8;
                    //Response.AddHeader("Content-Disposition", "attachment; filename=" + xlsPath);
                    
                    //Response.Flush();
                    //Response.Close();
                    //Response.End();

                    return File(xlsPath, "application/vnd.ms-excel", xlsFileName);

                }
                catch (Exception ex)
                {

                    StringBuilder errDetail = new StringBuilder();
                    errDetail.AppendLine(ex.Message);
                    errDetail.AppendLine(ex.StackTrace);
                    errDetail.AppendLine(htmlReportData);

                    try
                    {

                        System.IO.File.WriteAllText(destinationFolder + "Error-" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt", errDetail.ToString());

                    }
                    catch (Exception)
                    {
                        //failed completely
                        throw;
                    }

                }

                #endregion

                
               
            }

            return null;

        }
    }
}