﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlonsoBlanco.Reporting.Models
{
    public class ReportModel
    {

        public string Key { get; set; }
        public string Name { get; set; }
        public string Query { get; set; }
        public string Output { get; set; }

    }
}