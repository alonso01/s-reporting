﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlonsoBlanco.Reporting
{
    public class ReportsManager
    {

        protected static Dictionary<string, ReportElement> _instances;


        static ReportsManager()
        {
            _instances = new Dictionary<string, ReportElement>();
            ReportsConfigurationSection sec = (ReportsConfigurationSection)System.Configuration.ConfigurationManager.GetSection("reports");
            foreach (ReportElement i in sec.Instances)
            {
                _instances.Add(i.Key, i);
            }
        }

        public static ReportElement Instances(string key)
        {
            return _instances[key];
        }

        public static Dictionary<string, ReportElement> All()
        {
            return _instances;
        }

        public static bool IsValidReport(string key)
        {

            if (_instances.ContainsKey(key))
            {
                if (_instances[key].Active)
                {
                    return true;
                }else
                {
                    return false;
                }
            }else
            {
                return false;
            }

        }
        
        private ReportsManager()
        {
        }

    }
}