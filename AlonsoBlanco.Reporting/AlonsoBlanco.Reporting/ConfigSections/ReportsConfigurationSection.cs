﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlonsoBlanco.Reporting
{
    public sealed class ReportsConfigurationSection : ConfigurationSection
    {

        [ConfigurationProperty("", IsRequired = true, IsDefaultCollection = true)]
        public ReportInstanceCollection Instances
        {
            get { return (ReportInstanceCollection)this[""]; }
            set { this[""] = value; }
        }

    }

    public class ReportElement : ConfigurationElement
    {
        [ConfigurationProperty("key", IsRequired = true)]
        public string Key
        {
            get { return (string)base["key"]; }
            set { base["key"] = value; }
        }

        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get { return (string)base["name"]; }
            set { base["name"] = value; }
        }

        [ConfigurationProperty("query", IsRequired = true)]
        public string Query
        {
            get { return (string)base["query"]; }
            set { base["query"] = value; }
        }

        [ConfigurationProperty("output", IsRequired = true)]
        public string Output
        {
            get { return (string)base["output"]; }
            set { base["output"] = value; }
        }

        [ConfigurationProperty("active", IsRequired = true)]
        public bool Active
        {
            get { return bool.Parse(base["active"].ToString()); }
            set { base["active"] = value; }
        }
    }

    public class ReportInstanceCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new ReportElement();
        }
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ReportElement)element).Key;
        }
    }
}
