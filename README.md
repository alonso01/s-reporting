# README #

S-Reporting is a simple web application built with .NET framework that allows to do quick tabular reports, all managed from an xml file.

### What is this repository for? ###

* Quick summary



* Version 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies

* .NET Framework 4.6.2
* MVC 5
* JQuery 1.10+
* Bootstrap 3.6+
* Enterprise Library
* Hangfire
* 
* Database configuration

Configure based on your needs on the web.config file, using DefaultConnection string.

* Deployment instructions

Download this repo.
Build AlonsoBlanco.Reporting project.
Publish AlonsoBlanco.Reporting to the desired IIS server.
Setup the web.config file at your own needs. All reports are configured inside the reports section. User access is handled by forms authentication and the database server to connect is handled on DefaultConnection.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* support@alonsoblanco.me